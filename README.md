# ddbash #

** Dynamic DNS Bash Client **

`ddbash` is a Bash script conceived in order to making easier the process of updating, in a dynamic DNS service, IPv6 and also IPv5 addresses after every NetworkManager status change.

NetworkManager is a service commonly found in several GNU/Linux distributions like Ubuntu and Debian. In these distributions at least, according to its manual, NetworkManager triggers the execution, after each change in network status, of files found in directory `/etc/NetworkManager/dispatcher.d` with exclusive superuser permissions. This eliminates the need to set the script to be run by `crontab` or any other temporal repeating mechanism. Only when there is a network status change, `ddbash` will be run and it is also going to check if the IP address really changed before trying to send it by the Internet to the specified server.

Even though the goal is to be run by NetworkManager, and it is available a install script for that purpose, `ddbash` can be also run manually or with `crontab`.

### Dependencies ###

1. linux > 2.2
1. bash, coreutils, util-linux
1. curl (recommendable) ou wget (untested)
1. network-manager (optional, recommendable)

### Installing ###

Clone the repository and, with terminal, in the cloned directory, run`ddbash-install` as `root`.

Edit, also as `root`, the file `/opt/ddbash/ddbash.conf`, uncomment the lines regarding the full host name you want to be updated, your authentication _token_ given by the dynamic DNS service and the updating API URL, if different than the default value. This script has been tested only with dynv6.com service.

After network status changes, you may check the results in file `/var/log/ddbash.log`

#### Manual installing and running ####

If you do not wish using the installation script for NetworkManager, it is possible to copy the main program to any directory in `PATH` i.e. `~/bin`. The program expects the configuration file `ddbash.conf` being in the same directory.

Just run the program without any parameter. The log will be writen in `/var/log/ddbash.log` or, if there is no permission for that, in `~/ddbash.log`
# ddbash #

** Cliente DNS Dinâmico Bash **

O _script_ `ddbash` foi concebido visando facilitar a atualização em um serviço de DNS dinâmico de endereço IPv6 e subsidiariamente IPv4 a cada alteração de situação do NetworkManager.

NetworkManager é um serviço comumente encontrado em diversas distribuições GNU/Linux como Ubuntu e Debian. Ao menos nestas distribuições, de acordo com seu manual, o NetworkManager desencadeia a execução, a cada altereração de situação da rede, dos arquivos que encontra no diretório `/etc/NetworkManager/dispatcher.d`, que tenham permissão exclusiva do superusuário. Desta forma, dispensa-se a necessidade de colocar o _script_ para execução por `crontab` ou algum outro mecanismo de repetição temporal. Apenas quando houver alteração da situação da rede, `ddbash` será executado e ainda conferirá se há de fato uma alteração de endereço para então tentar enviá-lo pela Internet ao servidor especificado.

Mesmo visando ser executado pelo NetworkManager, algo para o qual está disponível um _script_ de instalação `ddbash-install`, também é possível utilizar `ddbash` manualmente ou com o `crontab`.

### Dependências ###

1. linux > 2.2
1. bash, coreutils, util-linux
1. curl (recomendável) ou wget (não testado)
1. network-manager (opcional, recomendável)

### Instalação ###

Clone o repositório e, com o terminal no diretório clonado, execute,`ddbash-install` como `root`.

Edite, também necessariamente como `root`, o arquivo `/opt/ddbash/ddbash.conf`, descomente as linhas referentes ao nome do _host_ que quer que seja atualizado, seu _token_ de autenticação no servidor e também o endereço da _API_ de atualização, caso diferente do padrão. Este _script_ foi testado apenas com o serviço dynv6.com.

Após a alteração de situação da rede, o resultado pode ser conferido em `/var/log/ddbash.log`

#### Instalação e utilização manual ####

Caso não deseje utilizar o _script_ de instalação voltado ao NetworkManager, é possível copiar o programa principal para qualquer diretório no `PATH` como por exemplo `~/bin`. O programa espera encontrar o arquivo de configuração `ddbash.conf` no mesmo diretório em que estiver armazenado.

Basta executar o programa sem qualquer parâmetro. O _log_ será gravado no `/var/log/ddbash.log` ou, caso não haja permissão para isto, em `~/ddbash.log`